@extends('layouts.main')


@section('container')
@include('partials.navbarabout')

<section class="main-content revealator-slideup revealator-once revealator-delay1">
    <div class="container">
        <div class="row">
            <!-- Personal Info Starts -->

            <div class="col-12 col-lg-10 col-xl-6">
                <div class="row">
                    <div class="col-12">
                        <h2 class="mb-0 text-uppercase custom-title ft-wt-400">Profil Lengkap</h2>
                        <p></p>
                        <p></p>
                        <p></p>
                    </div>
                    <div class="col-12 d-block d-sm-none">
                        <img src="img/img-mobile.jpg" class="img-fluid main-img-mobile" alt="my picture" />
                    </div>
                    <div class="col-6">
                        <ul class="about-list list-unstyled open-sans-font">
                            <li>
                                <span class="title"><h6>Nama lengkap </h6></span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">{{ $name1 }}</span>
                            </li>
                            <li>
                                <span class="title"><h6>Nama Panggilan</h6></span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">{{ $name2 }}</span>
                            </li>
                            <li>
                                <span class="title"><h6>Tempat Tanggal Lahir</h6></span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">{{ $ttl }}</span>
                            </li>
                            <li>
                                 <span class="title"><h6>Usia</h6></span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">{{ $age }}</span>
                            </li>
                            <li>
                                <span class="title"><h6>Alamat</h6></span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">{{ $alamat }}</span>
                            </li>
                        </ul>
                    </div>
                    <div class="col-6">
                        <ul class="about-list list-unstyled open-sans-font">
                            <li>
                                 <span class="title"><h6>WhatsApp</h6> </span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">{{ $tlp }}</span>
                            </li>
                            <li>
                                <span class="title"><h6>Email</h6></span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">{{ $email }}</span>
                            </li>
                            <li>
                                <span class="title"><h6>Jurusan</h6></span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">{{ $jurusan }}</span>
                            </li>
                            <li>
                                <span class="title"><h6>Fakultas</h6></span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">{{ $fakultas }}</span>
                            </li>
                            <li> <span class="title"><h6>Universitas</h6> </span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">{{ $universitas }}</span> </li>
                        </ul>
                    </div>

                </div>
            </div>
            <!-- Personal Info Ends -->
            <!-- Boxes Starts -->
            <div class="mt-5 col-12 col-lg-7 col-xl-6 mt-lg-0">
                <div class="row">
                    <div class="col-4">
                        <div class="box-stats with-margin">

                        </div>
                    </div>
                    <div class="col-4">
                        <img src="img/{{ $image }}" alt="<?= $name2; ?>" width="250">
                        </div>
                    </div>
                </div>
            </div>
            <!-- Boxes Ends -->
        </div>
        <hr class="separator">
        <!-- Skills Starts -->
        <div class="row">
            <div class="col-12">
                <h3 class="pb-4 mb-3 text-left text-uppercase pb-sm-5 mb-sm-0 text-sm-center custom-title ft-wt-600">My Skills</h3>
            </div>
            <div class="mb-3 col-6 col-md-3 mb-sm-5">
                <div class="c100 p25">
                    <span>25%</span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>
                <h6 class="mt-2 text-center text-uppercase open-sans-font mt-sm-4">Adobe premier pro</h6>
            </div>
            <div class="mb-3 col-6 col-md-3 mb-sm-5">
                <div class="c100 p89">
                    <span>89%</span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>
                <h6 class="mt-2 text-center text-uppercase open-sans-font mt-sm-4">Adobe Photoshop</h6>
            </div>
            <div class="mb-3 col-6 col-md-3 mb-sm-5">
                <div class="c100 p70">
                    <span>70%</span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>
                <h6 class="mt-2 text-center text-uppercase open-sans-font mt-sm-4">Adobe corel draw</h6>
            </div>
            <div class="mb-3 col-6 col-md-3 mb-sm-5">
                <div class="c100 p66">
                    <span>66%</span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>
                <h6 class="mt-2 text-center text-uppercase open-sans-font mt-sm-4">adobe Ilustration</h6>
            </div>
            <div class="mb-3 col-6 col-md-3 mb-sm-5">
            </div>
            </div>
             </div>
             </div>

            </div>
        </div>
        <!-- Skills Ends -->
        <hr class="mt-1 separator">
        <!-- Experience & Education Starts -->
        <div class="row">
            <div class="col-13">
                <h3 class="pb-5 mb-0 text-left text-uppercase text-sm-center custom-title ft-wt-600">RIWAYAT PENDIDIKAN</h3>
            </div>
            <div class="col-lg-6 m-15px-tb">
                <div class="resume-box">
                    <ul>
                        <li>
                            <div class="icon">
                            </div>
                            <span class="time open-sans-font text-uppercase">2016 - SMA</span>
                            <h5 class="poppins-font text-uppercase">SMA N1 Kubutambahan </h5>
                            <p class="open-sans-font">Mengambil jurusan IPA dan menempuh pendidikan selama 3 tahun  </p>
                        </li>

                    </ul>
                </div>
            </div>
            <div class="col-lg-6 m-19px-tb">
                <div class="resume-box">
                    <ul>
                        <li>
                            <div class="icon">
                            </div>
                            <span class="time open-sans-font text-uppercase">2019 - KULIAH</span>
                            <h5 class="poppins-font text-uppercase">Universitas Pendidikan Ganesha</h5>
                            <p class="open-sans-font">Mengambil Prodi Pendidikan Teknik Informatika, jurusan Teknik Infotatika, Fakultas Teknik Dan Kejuruan. dan masih menempuh pendidikan sampai saat ini
                        </li>


                </div>
            </div>
        </div>

@endsection
